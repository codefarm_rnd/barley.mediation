package barley.mediation.initializer;

import org.apache.axis2.description.Parameter;
import org.apache.axis2.engine.AxisConfiguration;

import barley.mediation.initializer.persistence.MediationPersistenceManager;

public class ServiceBusUtils {

	public static String generateFileName(String name) {
        return name.replaceAll("[\\/?*|:<> ]", "_") + ".xml";
    }
	
	public static MediationPersistenceManager getMediationPersistenceManager(AxisConfiguration axisCfg) {
        
        Parameter p = axisCfg.getParameter(
                ServiceBusConstants.PERSISTENCE_MANAGER);
        if (p != null) {
            return (MediationPersistenceManager) p.getValue();
        }

        return null;
    }          
}
