package barley.mediation.rest.api;

/**
 * Class <code>APIException</code> creates a custom exception for
 * Rest API admin
 *
 */
public class APIException extends Exception {

	private static final long serialVersionUID = -6272463911272868928L;

	public APIException() {
	}

	public APIException(String message) {
		super(message);
	}

	public APIException(Throwable cause) {
		super(cause);
	}

	public APIException(String message, Throwable cause) {
		super(message, cause);
	}

}