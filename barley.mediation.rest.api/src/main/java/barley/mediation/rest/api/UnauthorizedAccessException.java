package barley.mediation.rest.api;

/**
 * This class represents an exception, which is intended to be thrown when some client code accesses a portion of
 * business logic, i.e method or a block of code, that the it is not authorized or privileged to access.
 */
public class UnauthorizedAccessException extends RuntimeException {

    private static final long serialVersionUID = -7034897190745766941L;

    public UnauthorizedAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedAccessException(Throwable cause) {
        super(cause);
    }

    public UnauthorizedAccessException(String message) {
        super(message);
    }

}